<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookController extends Controller
{
    /*function stroe(Request $request){
        $book = new Books();
        $book->setAttibute("name",$request->name);
        $book->setAttibute("category",$request->category);
        $book->setAttibute("description",$request->description);
        if($book->svae){
            return true;
        }
    }*/

    //insert
    function stroe(Request $request)
    {
        if(Books::created($request->all())){
            return true;
        }
    }

    //update
    function update(Request $request, Books $book)
    {
        if($book->fill($request->all())->save()){
            return true;
        }
    }

    //select all
    function index()
    {
        $book = Books::all();
        return $book;
    }

    //select 1
    function show(Request $book)
    {
        return $book;
    }

    //delete
    function destroy(Books $book)
    {
        if($book->delete()){
            return true;
        }
    }
}
