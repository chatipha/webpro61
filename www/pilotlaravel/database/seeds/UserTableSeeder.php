<?php
/**
 * Created by PhpStorm.
 * User: iclassroom
 * Date: 28/11/2018 AD
 * Time: 10:24
 */
use Illuminate\Database\Seeder;
class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        Illuminate\Foundation\Auth\User::create(array(
            'name'     => 'phatthanaphong',
            'username' => 'joe',
            'email'    => 'joe@smsu.ac.th',
            'password' => Hash::make('awesome'),
        ));
    }

}
