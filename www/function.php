<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<script type="text/javascript">

		function makeUser(name, age) {
	  		return {
	    		name: name,
	    		age: age
	   		 // ...other properties
	  		};
		}

		let user = makeUser("John", 30);
		alert(user.name); // John

	</script>

</body>
</html>